import os
t= "./template/"

files = []
head = t+"header.html"
footer = t+"footer.html"
files.append("index.html");
files.append("post.html");
files.append("post1.html");
files.append("contact.html");
with open(head,'r') as h:
    head = h.read()

with open(footer,'r') as h:
    footer = h.read()

for file in files:
    with open(t+file, 'r') as infile:
        with open(file,'w') as outfile:
            page = infile.read()
            outfile.write(head+page+footer)
